﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace TestServer
{
    [HubName("MyHub")]
    public class Entry : Hub
    {
        public void Send(string name, string message)
        {
            Console.WriteLine($"{DateTime.Now} - {name}: {message}");
            this.Clients.Others.addMessage(name, message);
        }

        #region Overrides of HubBase

        public override Task OnConnected()
        {
            Console.WriteLine("Someone connected...");
            return base.OnConnected();
        }

        #endregion
    }
}
